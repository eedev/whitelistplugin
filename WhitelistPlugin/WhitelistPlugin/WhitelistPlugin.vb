﻿<Plugin(Authors:={"Processor"},
         Category:=PluginCategory.Admin,
         ChatName:="Whitelist",
         Description:="Allows you to allow only players on the rights list to join the world.",
         Version:="1.0.0.0")>
Public Class WhitelistPlugin
    Inherits Plugin(Of Player, WhitelistPlugin)

    Protected Overrides Sub OnEnable()
        myPlayerManager = PlayerManager
    End Sub

    Protected Overrides Sub OnConnect()

    End Sub

    Protected Overrides Sub OnDisable()
        myPlayerManager = Nothing
    End Sub

    Private myWhitelistEnabled As Boolean
    Private WithEvents myPlayerManager As IPlayerManager(Of Player)

    <Command("kickall", Group.Operator, AccessRight:=AccessRight.Owner)>
    Public Sub KickAllCommand(cmd As ICommand(Of Player), ParamArray reason As String())
        Dim r As String = String.Join(","c, reason)
        If r.Length = 0 Then r = "KickAll command invoked"
        For Each player In PlayerManager
            player.Kick(r)
        Next
    End Sub

    <Command("whitelist", Group.Operator, AccessRight:=AccessRight.Owner)>
    Public Sub KickAllCommand(cmd As ICommand(Of Player), state As String)
        Try
            Dim stateBool As Boolean = CBool(state)
            myWhitelistEnabled = stateBool

            If myWhitelistEnabled Then
                For Each player In PlayerManager
                    KickIfNotWhitelisted(player)
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub KickIfNotWhitelisted(player As Player)
        If player.Group <= Group.User AndAlso Game.AccessRight >= AccessRight.Owner AndAlso player.Username <> Game.MyPlayer.Username Then
            player.Kick("You are not on the whitelist")
        End If
    End Sub

    Private Sub myPlayerManager_Join(sender As Object, e As Player) Handles myPlayerManager.Join
        If myWhitelistEnabled Then
            AddHandler e.UserDataReady,
                Sub()
                    KickIfNotWhitelisted(e)
                End Sub
        End If
    End Sub
End Class
